<?php require('app/rsvp.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Anne &amp; Mikes Wedding - RSVP</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="style/global.css">
  <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
  <link rel="icon" type="image/png" href="favicon.png">
</head>
<body>

  <div class="container">
    <header>
      <a href="/">
        <img src="img/top-flowers.png" alt="A big bouqet of flowers" width="60%" class="top-flowers">
      </a>
      <h1>
        RSVP
      </h1>
      <img src="img/bottom-flowers.png" alt="A flower" width="30%" class="bottom-flowers">
    </header>

    <article>
      <section>
        <!-- TODO: Do we need some introduction text here? -->
        <form action="/rsvp" method="POST">
          <div class="form-group">
            <label for="your-name">What is your name?</label>
            <input type="text" id="your-name" name="your_name" autofocus>
          </div>
          <div class="form-group">
            <label for="your-email">What is your email address?</label>
            <input type="text" id="your-email" name="your_email">
          </div>
          <div class="form-group">
            <label for="how-many-people">How many are in your party?</label>
            <input type="text" id="how-many-people" name="how_many_people">
          </div>
          <div class="form-group">
            <label for="number-able-to-attend">How many of you are able to attend?</label>
            <input type="text" id="number-able-to-attend" name="number_able_to_attend">
          </div>
          <div class="form-group">
            <label for="notes">Text for note field <span class="optional">(optional)</span></label>
            <textarea name="notes" id="notes" cols="30" rows="4"></textarea>
          </div>
          <div class="actions">
            <input type="submit" value="Submit RSVP">
          </div>
        </form>
      </section>
    </article>
  </div>

  <?php include('parts/footer.php'); ?>
</body>
</html>
