<footer>
  <p>
    <img src="/img/bottom-flowers.png" alt="" width="50%" class="center-img bottom-flowers">
  </p>
  <p class="polaroids">
    <?php
      $polaroids = array(
        'marquee',
        'anne-drink',
        'glasto-sunset',
        'mike-anne',
        'field',
        'mike-hat',
        'richmond',
      );
    ?>
    <?php foreach ($polaroids as $img): ?>
      <img src="/img/polaroids/<?php echo $img; ?>.png" class="polaroid polaroid-<?php echo $img; ?>" alt="">
    <?php endforeach; ?>
  </p>
</footer>
