<?php

class DB {
  private $mysqli;
  
  function __construct() {
    $hostname = 'localhost';
    $username = 'root';
    $password = 'FrezEth4';
    $database = 'wedding';
    $this->connection = new mysqli($hotname, $username, $password, $database);

    if (mysqli_connect_errno()) {
      printf("Connect failed: %s\n", mysqli_connect_error());
      exit();
    }
  }

  function query($sql) {
    $result = $this->connection->query($sql);
    if (!$result) {
      printf("Error: %s\n", $this->connection->sqlstate);
      exit;
    }
    $this->connection->close();
    return $result;
  }

  public function insert($table, $args) {
    // sanitise args
    foreach ($args as $key => $value) {
      $args[$key] = "'" . $this->connection->real_escape_string($value) . "'";
    }

    // turn args into sql
    $columns = implode(", ", array_keys($args));
    $values  = implode(", ", $args);
    $sql = "INSERT INTO `{$table}`({$columns}) VALUES ({$values})";

    // do it!
    $query = $this->connection->query($sql);

    // throw an exception if there is an error
    if (!$query) {
      throw new Exception(sprintf("Error: %s\n", $this->connection->error), 1);
    }

    // get the number of affected rows, close and return
    $result = $this->connection->affected_rows;
    $this->connection->close();
    return $result;
  }

  public function all($table) {
    return $this->query("SELECT * FROM {$table}");
  }
}

class Model {
  private $db;
  protected $table;

  function __construct($table) {
    $this->db = new DB();
    $this->table = $table;
  }

  public function insert($args) {
    return $this->db->insert($this->table, $args);
  }

  public function all() {
    return $this->db->all($this->table, $args);
  }
}

class Rsvp extends Model {
  function __construct() {
    parent::__construct('rsvps');
  }
}
