<?php
require('app.php');

if ($_POST) {
  $rsvp = new Rsvp;

  $rsvp->insert(array(
    'name' => $_POST['your_name'],
    'email' => $_POST['your_email'],
    'party_size' => $_POST['how_many_people'],
    'number_able_to_attend' => $_POST['number_able_to_attend'],
    'notes' => $_POST['notes'],
  ));

  header("Location: /thank-you", 303);
  die();
}
