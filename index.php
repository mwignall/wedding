<!DOCTYPE html>
<html lang="en">
<head>
  <title>Anne &amp; Mikes Wedding</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="style/global.css">
  <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
  <link rel="icon" type="image/png" href="favicon.png">
</head>
<body>

  <header>
    <a href="/">
      <img src="img/top-flowers.png" alt="A big bouqet of flowers" width="60%" class="center-img top-flowers">
    </a>
    <h1>
      Anne McGouran &amp; Michael Wignall <br>
      17th July 2017
    </h1>
    <img src="img/bottom-flowers.png" alt="" width="30%" class="center-img bottom-flowers">
  </header>

  <article>
    <section class="rsvp">
      <h2>RSVP</h2>
      <p>
        Please return your reply card to <br>
        51 Glaisdale Avenue <br>
        Middlesbrough <br>
        TS5 7PF
      </p>
      <p>
        Please let us know if you need transport <br>
        or you can RSVP her on the website
      </p>
      <a href="/rsvp" class="btn">
        RSVP online
      </a>

      <p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2325.0597661684637!2d-1.6675326842846454!3d54.35883198020369!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNTTCsDIxJzMxLjgiTiAxwrAzOSc1NS4yIlc!5e0!3m2!1sen!2suk!4v1486412150014" width="100%" height="290" frameborder="0" style="border:0" allowfullscreen></iframe>
      </p>
    </section>

    <section>
      <p class="hidden-md">
        <img src="img/bunch-horizontal.png" alt="" width="60" class="center-img flower-divider">
      </p>
      <h2>
        Information
      </h2>
      <p>
        The wedding will take place at St Joseph &amp; Francis Xavier, 25 Victoria Road, Richmond, North Yorkshire, DL10 4AS.
      </p>
      <p>
        The reception is at Bethel Cottage, Tunstall. We will be providing as much transport as possible as there will be little room for parking
        Please inform us before if you are driving to the reception so we can make arrangements for you.
      </p>
      <p>
        As the marquee is on a field access to it is on a track, if the weather has not been good that week we will advise our guests to wear/bring sensible footwear, but we hope for sunshine!
      </p>
      <p>
        The reception will end at midnight, we are providing transport to local hotels and back to Middlesbrough (for our evening guests) at 12:30am. <br>
        If you need any more information please don't hesitate to contact us, we are looking forward to spending our day with you!
      </p>
    </section>

    <section>
      <p class="hidden-md">
        <img src="img/bunch-horizontal.png" alt="" width="60" class="center-img flower-divider">
      </p>
      <h2>
        Evening Reception Guests
      </h2>
      <p>
        Transport will be provided from the Endeavour pub, Middlesbrough, TS5 7NQ at 6:30pm. Please let us know if you need transport
      </p>

      <p class="hidden-md">
        <img src="img/bunch-horizontal.png" alt="" width="60" class="center-img flower-divider">
      </p>
      <h2>Gifts</h2>
      <p>
        Our gift list will be available in April, all details will be posted on the website. Thank you
      </p>
    </section>

    <section>
      <p class="hidden-md">
        <img src="img/bunch-horizontal.png" alt="" width="60" class="center-img flower-divider">
      </p>
      <h2>Hotels</h2>
      <p>
        The nearest hotels to the wedding reception are:
      </p>
      <ul class="hotels">
        <?php
          $hotels = array(
            array(
              'title' => 'The Premier Inn',
              'url' => 'http://www.premierinn.com/gb/en/hotels/england/north-yorkshire/catterick-garrison/catterick-garrison.html',
              'location' => 'Catterick',
              'phone' => '0871 527 9568',
            ),
            array(
              'title' => 'Kings Head Hotel',
              'url' => 'http://www.kingsheadrichmond.co.uk/',
              'location' => 'Richmond',
              'phone' => '01748 850220',
            ),
            array(
              'title' => 'Holiday Inn',
              'url' => 'https://www.holidayinn.com/hotels/gb/en/darlington/xvgdl/hoteldetail?qAdlt=1&qBrs=6c.hi.ex.rs.ic.cp.in.sb.cw.cv.ul.vn.ki&qChld=0&qFRA=1&qGRM=0&qIta=99603206&qPSt=0&qRRSrt=rt&qRef=df&qRms=1&qRpn=1&qRpp=20&qSHp=1&qSmP=3&qSrt=sBR&qWch=0&srb_u=1&icdv=99603206&siclientid=1935&sitrackingid=443439000&dp=true&glat=SEAR',
              'location' => 'Scotch Corner',
              'phone' => '01748 850900',
            ),
          );
        ?>
        <?php foreach ($hotels as $hotel): ?>
          <li>
            <a href="<?php echo $hotel['url']; ?>">
              <?php echo $hotel['title']; ?>
            </a>
            <span class="location"><?php echo $hotel['location']; ?></span>
            <span class="phone"><?php echo $hotel['phone']; ?></span>
          </li>
        <?php endforeach; ?>
      </ul>
      <p>
        There are also some small boutique-style hotels in Richmond and the surrounding areas.
      </p>
    </section>

    <section class="wide">
      <p class="hidden-md">
        <img src="img/bunch-horizontal.png" alt="" width="60" class="center-img flower-divider">
      </p>
      <h2>Contact us</h2>
      <p>
        Mike: 07813961160 <br>
        Anne: 07813961160
      </p>
    </section>
  </article>

  <?php include('parts/footer.php'); ?>
</body>
</html>
