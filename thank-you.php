<!DOCTYPE html>
<html lang="en">
<head>
  <title>Anne &amp; Mikes Wedding</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="style/global.css">
  <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
  <link rel="icon" type="image/png" href="favicon.png">
</head>
<body>

  <header>
    <a href="/">
      <img src="img/top-flowers.png" alt="A big bouqet of flowers" width="60%" class="center-img top-flowers">
    </a>
    <h1>
      Thank you
    </h1>
    <img src="img/bottom-flowers.png" alt="" width="30%" class="center-img bottom-flowers">
  </header>

  <article>
    <section class="rsvp">
      <p>
        Thank you for your RSVP<br>
      </p>
      <p>
        If you haven't had a look already, please have a look at our
        <!-- TODO: link to the gift list -->
        <a href="#">wedding gift list</a>
      </p>
      <p>
        If you'd like to have another look at the details you can go back to the
        <a href="/">home page</a>
      </p>
    </section>
  </article>

  <?php include('parts/footer.php'); ?>
</body>
</html>
